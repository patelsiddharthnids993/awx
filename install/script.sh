echo "**************************************** Installation statrted **********************************"
echo "=================================================================================================="
echo "Uninstall the old docker environment"
sleep 2

sudo apt update
sudo apt -y remove --purge docker docker-engine docker.io containerd runc
sudo apt -y autoremove
sudo apt -y autoclean

echo "=================================================================================================="

echo "=================================================================================================="
echo "Update the software repository and get docker package"
sleep 2

sudo apt update
sudo apt-get -y install ca-certificates curl gnupg lsb-release
sudo mkdir -m 0755 -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

echo "=================================================================================================="

echo "=================================================================================================="
echo "Update the package again"
sleep 2

sudo apt -y update
sudo apt -y autoremove
sudo apt -y autoclean

echo "=================================================================================================="

echo "=================================================================================================="
echo "Install docker"
sleep 2

sudo apt-get -y install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

echo "=================================================================================================="

echo "=================================================================================================="
echo "Install the prerequisite packages"
sleep 2

sudo apt-get -y install python3 python3-pip virtualenv

echo "=================================================================================================="

echo "=================================================================================================="
echo "Installation of ansible and docker-compose using python"
sleep 2

python3 -m pip install pip --upgrade
python3 -m pip install 'ansible==2.10'
python3 -m pip install docker-compose

echo "=================================================================================================="

echo "=================================================================================================="
echo "Install git"
sleep 2

sudo apt -y install git

echo "=================================================================================================="

echo "=================================================================================================="
echo "Copy the AWX project"
sleep 2

git clone -b 15.0.1 https://github.com/ansible/awx.git awx_15.0.1

echo "=================================================================================================="

echo "=================================================================================================="
echo "Changing the docker permission"
sleep 2

sudo chown ubuntu:ubuntu /var/run/docker.sock

echo "=================================================================================================="

echo "=================================================================================================="

echo "Changing the docker setting"
sleep 2

cp ../../.local/lib/python3.10/site-packages/docker/utils/utils.py ../../.local/lib/python3.10/site-packages/docker/utils/utils.py_orig
sed -i 's/def kwargs_from_env(environment=None)/def kwargs_from_env(environment=None, ssl_version=None)/g' ../../.local/lib/python3.10/site-packages/docker/utils/utils.py
echo "=================================================================================================="

echo "=================================================================================================="

echo "Install AWX"
sleep 2

$HOME/.local/bin/ansible-playbook -i awx_15.0.1/installer/inventory awx_15.0.1/installer/install.yml

echo "=================================================================================================="

echo "=================================================================================================="
echo "Create an SSH key pair to access the endpoint you are about to create"
sleep 2

ssh-keygen -t ed25519 -N "" -f ssh-keypair -C "ssh-key to access servers"

echo "=================================================================================================="

echo "=================================================================================================="
echo "Creating the endpoint"
sleep 2

$HOME/.local/bin/docker-compose -f docker-compose.yml build
$HOME/.local/bin/docker-compose -f docker-compose.yml up -d

echo "=================================================================================================="

echo "=================================================================================================="
echo "Creating ssh config file"
sleep 2

cat << EOF > ~/.ssh/config
user ubuntu
IdentityFile ~/.ssh/ssh-keypair
StrictHostKeyChecking no
EOF

echo "=================================================================================================="

echo "=================================================================================================="
echo "Creating inventory file"
sleep 2

cp my_inventory_orig my_inventory

for node in target01 target02 target03 target04
do
 ip=$(docker inspect --format '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ${node})
 sed -i "s/${node} ansible_host={${node}_ipaddr}/${node} ansible_host=${ip}/" my_inventory
 printf "\nHost ${node}\n Hostname ${ip}\n" >> ~/.ssh/config
done

echo "=================================================================================================="

echo "=================================================================================================="
echo "Copy public/private keys to endpoints and to local .ssh dir"
sleep 2

cp ssh-keypair ~/.ssh/

for node in target01 target02 target03 target04
do
 docker cp ssh-keypair.pub ${node}:/home/ubuntu/.ssh/authorized_keys
done

echo "=================================================================================================="

echo "**************************************** Installation Completed **********************************"
